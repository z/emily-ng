Emily
=====

Emily is a communication engine written in Python that allows rich connections
between developers and their communities.  Emily aims to be protocol and backend
agnostic, allowing you to choose the solutions that work best for your team and
your users.

