=======================
 Emily Project Charter
=======================
:Authors:
  * **A. Wilcox**, *project evangelist*
  * **Elizabeth Myers**, *open-source evangelist*
  * **Horst Burkhardt**, *experience evangelist*
:Organization:
  Wilcox Technologies, LLC
:Version:
  1.0
:Status:
  Draft
:Copyright:
  © 2015-2017 Wilcox Technologies LLC.  NCSA open source licence.

.. NOTE:: This document contains information about a forthcoming product from
          Wilcox Technologies.  The information contained within may not reflect
          the final product and may change rapidly during development processes.
          Always ensure you have the latest copy of all documents before raising
          any concerns you may have.

.. NOTE:: As of 2014, project document revision history is no longer tracked
          within the document itself.  For revision history of this document
          you must consult the source control software used for the associated
          project.




Business requirements
=====================

Background
----------
There is no single solution for notifying interested parties about software
development processes, and one is desperately needed.

In 2014 alone, projects built by Wilcox Technologies had at least three
different notification engines just for updating IRC.  Every new system seems to
require another local mailer daemon to be installed and configured.  The overall
burden to a development organisation of even a small size is astronomical.  Even
"off-premises" cloud solutions like GitHub for code and issue tracking, JIRA,
Pivotal Tracker, and their ilk require a lot of configuration and continued
maintenance to notify developers, testers, users, and others of issues that
matter to them.

A new system is therefore desired that can lessen the burden of configuration
and maintenance.  It would ideally allow connection with multiple producers and
send the information on to multiple consumers, allowing flexible and timely
notification.


Business opportunity
--------------------
Existing solutions are incredibly specific and hard to use.  Each system that is
currently on the market requires its own configuration and maintenance.  When a
new system is introduced, it requires its own solution.

We aim to resolve this with "Emily".  We feel that this solution will not only
aid our own development efforts, allowing us to be more effective and efficient
in our work, but that it will additionally be able to help other organisations.
We can host free instances for open-source projects, gaining trust from the
community at large and allowing us to enhance our brand recognition.  We can
sell the product to other ISVs and software development firms/contractors.

We also aim to integrate "Emily" heavily with Spark when it is released, making
it even easier for customers to migrate to Spark by supporting their existing
"Emily" infrastructure.


Objectives / success critera
----------------------------
* Reduce bug backlog on internal projects by at least 30%.
* Have at least 3 positive reviews written in software development / technology
  publications by Q4 2015.
* Reduce configuration and maintenance burden on internal systems.




Solution vision
===============

Vision statement
----------------
For software development teams who want to have timely, effectual notifications
about the entire lifecycle of development, "Emily" is a communications hub and
infrastructure that will enable teams and interested parties to be notified in
whatever way is convenient for them.  Unlike other disparate solutions that
require repetitive configuration and separate maintenance when systems change,
our system will be centralised and allow for fast and easy maintenance whenever
required.


Major features
--------------
#. Connect to existing source control, issue tracker, and project management
   systems and provide notification on changes and events occurring to tracked
   projects.

#. Send notifications via multiple systems:

   #. Email.

   #. IRC (Internet Relay Chat).

   #. AIM (AOL Instant Messenger) and XMPP (Extensible Messaging and Presence
      Protocol).

   #. DCP (Domain Chat Protocol).

   #. SMS (Short Message Service) and MMS (Multimedia Message Service).

   #. Mobile clients (iOS, Android).

#. Support granular notification settings: per-project, per-team, and per-
   individual.

#. Feature a simple Web-based interface for maximum compatibility and usability.


Assumptions
-----------
We have assumed that any issue tracker and source control system that will be
supported for notification allows an extension or plug-in of some kind that will
allow us to 'hook' events to send it to the central notification daemon.  Most
popular issue trackers such as Bugzilla, Trac, Redmine, GitHub, and Pivotal all
support this.  Support for git, svn (Subversion), and hg (Mercurial) is also
assured.

We have assumed there is actually interest in the community to have easy-to-use
tools for notifying developers, testers, and other key people.  Some developers
will probably not like being bothered :)


Dependencies
------------
* **AIM/XMPP (2.3)**:
    For AIM and XMPP support, we will require the Pounce project to already be
    completed, so this will not be considered for v1.0.
* **DCP (2.4)**:
    We cannot target an emerging protocol in a production environment, so this
    will also be a "future goal" that will not be considered for v1.0.
* **SMS/MMS (2.5)**:
    We will need to find an SMS/MMS gateway that we can easily integrate.




Project Scope and Limitations
=============================

Scope of initial release (v1)
-----------------------------
Foo


Scope of next release (v2)
--------------------------
Foo


Scope of future releases
------------------------
Foo


Limitations and exclusions
--------------------------
Bar




Context
=======
