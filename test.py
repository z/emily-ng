from emily.inp.gitlab import GitLab
from emily.inp.github import GitHub

from emily.out.irc import IRC

import emily.core


emily.core.register_in(GitLab)
emily.core.register_in(GitHub)
emily.core.register_out(IRC)
emily.core.run()

