"""Basic protocol handling for Emily."""

from emily.protocols.http import HTTPProtocol

__all__ = [HTTPProtocol]
