from aiohttp import web
import asyncio
from taillight import Signal

from emily import config

http_handle = Signal(('http', 'handle'))

class HTTPProtocol():
    def __init__(self, loop):
        if 'http' not in config:
            config['http'] = {'port': 80, 'ssl': False}

        if config['http'].get('ssl', False):
            # handle SSL
            pass

        # listen on port
        self.app = web.Application(loop=loop)
        self.app.router.add_route('GET', '/{project}', self.handler)
        self.app.router.add_route('POST', '/{project}', self.handler)
        self.app.router.add_route('GET', '/{grp}/{project}', self.handler)
        self.app.router.add_route('POST', '/{grp}/{project}', self.handler)

        self.server = loop.create_server(self.app.make_handler(),
                                         '0.0.0.0', int(config['http']['port']))

    def coros(self):
        return [self.server]

    @asyncio.coroutine
    def handler(self, request):
        """Handle requests."""
        responses = yield from http_handle.call_async(request)

        if any(responses):
            return web.Response(body=b'Okay')
        return web.Response(body=b'Not handled')
