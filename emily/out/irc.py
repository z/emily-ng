import asyncio
from collections import defaultdict
from PyIRC.extensions import bot_recommended
from PyIRC.io.asyncio import IRCProtocol
from PyIRC.signal import event
from PyIRC.util.version import VERSIONSTR

from emily import config
from emily.core import push_event, merge_event, pipe_event, issue_open_event, issue_close_event


class IRCNetwork(IRCProtocol):
    """The basic IRC network representation.  Each network has an instance."""
    def __init__(self, *args, **kwargs):
        super(IRCNetwork, self).__init__(*args, **kwargs)

    @event('commands', 'NOTICE')
    def on_message(self, caller, line):
        if line.params[0] == self.nick and line.params[-1].upper() == 'REHASH':
            config.reload()


class IRC:
    """The IRC output plugin.
    
    Config:
        networks = comma separated list of networks.  For each network:
            network_server = IP or hostname
            network_port = IRC port
            network_ssl = True/False
            network_nick = Nickname to use
            network_gecos = GECOS
            network_username = Username to auth with (or not present)
            network_password = Password to auth with (or not present)
            network_channels = comma separated list of channels.  For each:
            network_channel_projects = comma separated list of projects.
            """
    def __init__(self, loop):
        push_event.add(self.push)
        merge_event.add(self.merge)
        pipe_event.add(self.pipe)
        issue_open_event.add(self.issue_open)
        issue_close_event.add(self.issue_close)

        myconf = config['irc']
        self.networks = [network.strip()
                         for network in myconf['networks'].split(',')]
        self.network_instances = {}
        self.projects = defaultdict(list)

        url = 'https://code.foxkit.us/wilcox-tech/emily-ng'
        my_ver = 'Emily ({}) on PyIRC v{}'.format(url, VERSIONSTR)

        for network in self.networks:
            args = {
                'serverport': (myconf[network + '_server'],
                               int(myconf[network + '_port'])),
                'ssl': myconf[network + '_ssl'].lower() == 'true',
                'nick': myconf[network + '_nick'],
                'gecos': myconf[network + '_gecos'],
                'extensions': bot_recommended,
                'ctcp_version': my_ver,
                'join': [],
            }

            args['username'] = myconf.get(network + '_username',
                                          myconf[network + '_nick'])

            if network + '_username' in myconf:
                args['sasl_username'] = myconf[network + '_username']
                args['sasl_password'] = myconf[network + '_password']

            for channel in [ch.strip()
                            for ch in myconf[network + '_channels'].split(',')]:
                args['join'].append('#' + channel)
                proj_setting = network + '_' + channel + '_projects'
                for project in [proj.strip()
                                for proj in myconf[proj_setting].split(',')]:
                    self.projects[project].append('{}:{}'.format(network,
                                                                 '#' + channel))

            netinst = IRCNetwork(**args)
            self.network_instances[network] = netinst

    def coros(self):
        return [inst.connect() for inst in self.network_instances.values()]

    def _determine_channels(self, project: str) -> set:
        """Determine the channels that are interested in an event."""
        channels = set(self.projects[project] +
                       self.projects[project.split('/', 1)[0] + '/*'] +
                       self.projects['*/*'])
        return channels

    def _send_to_proj_channels(self, project: str, msgs: list):
        """Send a list of messages to channels interested in a project's events."""
        channels = self._determine_channels(project)
        for channel in channels:
            network, irc_chan = channel.split(':', 1)
            for msg in msgs:
                self.network_instances[network].send('PRIVMSG', [irc_chan, msg])

    def push(self, project, push):
        commit_msgs = []

        push_msg = '\x02{}\x02: {} pushed '.format(project, push['user']['name'])
        count = len(push['commits'])
        if count > 1:
            push_msg += '{} commits '.format(count)
        else:
            push_msg += 'a commit '
        push_msg += 'to the \x02{}\x02 branch:'.format(push['branch'])
        commit_msgs.append(push_msg)

        for commit in push['commits']:
            try:
                commit_msg = commit['msg'].splitlines()[0]
            except IndexError:
                commit_msg = commit['msg']
            msg = '{} ( {} ) by \x02{}\x02: {}'.format(commit['id'][:7],
                                                     commit['url'],
                                                     commit['author']['name'],
                                                     commit_msg)
            commit_msgs.append(msg)

            if len(commit_msgs) == 3 and count > 3:
                msg += ' [... and {} more]'.format(count - 3)
                commit_msgs.pop()
                commit_msgs.append(msg)
                break

        self._send_to_proj_channels(project, commit_msgs)

    def merge(self, project, merge):
        msg = '\x02{}\x02: {} opened a merge request for the \x02{}\x02 branch: {}'
        msg = msg.format(project, merge['user']['name'], merge['branch'], merge['url'])
        self._send_to_proj_channels(project, [msg])

    def pipe(self, project, pipe):
        msg = '\x02{}\x02: Pipeline #{} for branch \x02{}\x02 status: {} - {}'
        msg = msg.format(project, pipe['id'], pipe['branch'], pipe['status'], pipe['url'])
        self._send_to_proj_channels(project, [msg])

    def issue_common(self, project, issue, action):
        msg = '\x02{}\x02: {} {} issue #{} (\x02{}\x02): {}'
        msg = msg.format(project, issue['user']['name'], action, issue['id'], issue['title'],
                         issue['url'])
        self._send_to_proj_channels(project, [msg])

    def issue_open(self, project, issue):
        self.issue_common(project, issue, 'filed')

    def issue_close(self, project, issue):
        self.issue_common(project, issue, 'closed')
