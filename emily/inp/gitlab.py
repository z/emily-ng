import asyncio

from emily.core import (push_event, merge_event, pipe_event,
                        issue_open_event, issue_close_event)
from emily.protocols.http import http_handle, HTTPProtocol

SKIP_PIPE = ('pending', 'running',)

class GitLab:
    """ Processes information from GitLab Web Hooks. """
    PROTOCOLS = (HTTPProtocol,)

    def __init__(self):
        http_handle.add(self.http_handle)

    @asyncio.coroutine
    def http_handle(self, request):
        if 'X-Gitlab-Event' not in request.headers:
            return False

        json = yield from request.json()
        if 'object_kind' not in json:
            return False  # why?!

        if json['object_kind'] == 'push':
            push_info = {}
            push_info['user'] = {'name': json['user_name'],
                                 'email': json['user_email']}
            push_info['branch'] = json['ref'].replace("refs/heads/", "", 1)
            push_info['commits'] = []
            for commit in json['commits']:
                push_info['commits'].append({'id': commit['id'],
                                             'url': commit['url'],
                                             'msg': commit['message'],
                                             'author': commit['author']})

            project = request.match_info.get('grp', 'default') + '/' + \
                      request.match_info.get('project')

            yield from push_event.call_async(project, push_info)

            return True
        elif json['object_kind'] == 'merge_request':
            if json['object_attributes'].get('action', 'open') != 'open':
                return True

            merge_info = {}
            merge_info['user'] = {'name': json.get('user', {}).get('name', '')}
            merge_info['branch'] = json['object_attributes']['source_branch']
            merge_info['url'] = json['object_attributes']['url']

            project = request.match_info.get('grp', 'default') + '/' + \
                      request.match_info.get('project')

            yield from merge_event.call_async(project, merge_info)

            return True
        elif json['object_kind'] == 'pipeline':
            if json['object_attributes'].get('status', 'pending') in SKIP_PIPE:
                return True
            pipe_info = {}
            pipe_info['branch'] = json['object_attributes'].get('ref', 'Unknown')
            pipe_info['status'] = json['object_attributes'].get('status')
            pipe_info['id'] = str(json['object_attributes'].get('id'))
            pipe_info['url'] = json['project'].get('web_url')
            if pipe_info['url']:
                pipe_info['url'] = pipe_info['url'] + '/pipelines/'
                pipe_info['url'] = pipe_info['url'] + pipe_info['id']

            project = request.match_info.get('grp', 'default') + '/' + \
                      request.match_info.get('project')

            yield from pipe_event.call_async(project, pipe_info)
        elif json['object_kind'] == 'issue':
            attrs = json['object_attributes']
            action = attrs.get('action', 'open')
            if action in ('open', 'reopen'):
                event = issue_open_event
            elif action == 'close':
                event = issue_close_event
            else:
                return True  # Silently ignore 'update' events.

            issue_info = {}
            issue_info['user'] = {'name': json.get('user', {}).get('name', '')}
            # Not a typo: 'iid' is the Issue ID, while 'id' is the project object ID
            issue_info['id'] = str(attrs.get('iid'))
            issue_info['title'] = attrs.get('title', 'Missing title')
            issue_info['url'] = attrs.get('url')
            project = request.match_info.get('grp', 'default') + '/' + \
                      request.match_info.get('project')

            yield from event.call_async(project, issue_info)
        else:
            return False
