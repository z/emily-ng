import asyncio

from emily.core import push_event
from emily.protocols.http import http_handle, HTTPProtocol

class GitHub:
    """ Processes information from GitHub Web Hooks. """
    PROTOCOLS = (HTTPProtocol,)

    def __init__(self):
        http_handle.add(self.http_handle)

    @asyncio.coroutine
    def http_handle(self, request):
        if 'X-GitHub-Event' not in request.headers:
            return False

        json = yield from request.json()
        if request.headers['X-GitHub-Event'] != 'push':
            return False  # we only do push right now

        push_info = {}
        push_info['user'] = {'name': json['pusher']['name'],
                             'email': json['pusher']['email']}
        push_info['branch'] = json['ref'].split('/')[-1]
        push_info['commits'] = []
        for commit in json['commits']:
            push_info['commits'].append({'id': commit['id'][:8],
                                         'url': commit['url'],
                                         'msg': commit['message'],
                                         'author': commit['author']})

        project = request.match_info.get('grp', 'default') + '/' + \
                  request.match_info.get('project')

        yield from push_event.call_async(project, push_info)

        return True
